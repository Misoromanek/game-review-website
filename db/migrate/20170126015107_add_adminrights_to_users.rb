class AddAdminrightsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :adminrights, :boolean
  end
end
