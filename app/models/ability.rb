class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    can [:index, :new, :create, :show], User
    can [:index, :show], Post
    can [:create], Comment
    if user.adminrights?
      can [:index, :new, :create, :show, :update, :destroy, :promote], User
      can [:index, :show, :new, :create, :update, :destroy], Post
      can [:create, :destroy, :reveal], Comment
    else
      can [:index, :new, :create, :show, :update], User
      can [:index, :show, :new, :create, :update], Post
      can [:create, :destroy, :reveal], Comment
    end
  end
end
