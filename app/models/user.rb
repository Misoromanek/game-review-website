class User < ApplicationRecord
  authenticates_with_sorcery!

  validates :password, length: { minimum: 4 }, if: -> { new_record? || changes[:crypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }

  validates :email, uniqueness: true, length: { minimum: 4 }
  validates :more, length: { maximum: 300 }
  has_many :posts
  
  ratyrate_rater
end
