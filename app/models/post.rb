class Post < ActiveRecord::Base
  has_many :comments
  belongs_to :user
  ratyrate_rateable 'gameplay', 'graphics', 'story', 'uniqueness', 'addictiveness'

  def approval?
    commstats == 2 or commstats == 3
  end

  def enabled?
    commstats == 1 or commstats == 2
  end
end
