class RaterController < ApplicationController
  before_action :require_login

  def create
    obj = params[:klass].classify.constantize.find(params[:id])
    obj.rate params[:score].to_f, current_user, params[:dimension]
  end
end
