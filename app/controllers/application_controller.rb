class ApplicationController < ActionController::Base
  before_action do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to :mainpages, alert: exception.message
  end
  before_action :require_login
  protect_from_forgery with: :exception
end
