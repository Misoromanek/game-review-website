class CommentsController < ApplicationController
  skip_before_action :require_login, only: [:create]

  load_and_authorize_resource except: [:create]

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(params.require(:comment).permit(:text, :user_email))
    @comment.shown = false
    @comment.save
    redirect_to @post
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    redirect_to @comment.post
  end

  def reveal
    @comment = Comment.find(params[:id])
    @comment.shown = true
    @comment.save
    redirect_to @comment.post
  end
end
