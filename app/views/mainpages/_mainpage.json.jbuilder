json.extract! mainpage, :id, :created_at, :updated_at
json.url mainpage_url(mainpage, format: :json)