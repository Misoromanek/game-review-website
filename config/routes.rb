Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  post '/posts/:post_id/comments/:id' => 'comments#reveal', :as => 'reveal'
  post '/users/:user_id' => 'users#promote', :as => 'promote'

  root to: 'mainpages#index'
  resources :user_sessions
  resources :users
  resources :mainpages
  resources :posts do
    resources :comments
  end

  get 'login' => 'user_sessions#new', :as => :login
  post 'logout' => 'user_sessions#destroy', :as => :logout
end
